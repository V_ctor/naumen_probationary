package springboot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import springboot.configuration.RepositoryConfiguration;
import springboot.domain.Article;
import springboot.domain.Chapter;
import springboot.repositories.ArticleRepository;
import springboot.repositories.ChapterRepository;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {RepositoryConfiguration.class})
@WebAppConfiguration
public class SpringBootWebApplicationTests {

	private ChapterRepository chapterRepository;
	private ArticleRepository articleRepository;

	@Autowired
	public void setChapterRepository(ChapterRepository chapterRepository, ArticleRepository articleRepository) {
		this.chapterRepository = chapterRepository;
		this.articleRepository = articleRepository;
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testSaveChapter(){
		Chapter chapter1 = new Chapter();
		chapter1.setShortText("Description 1 ");

		//save chapter, verify has ID value after save
		assertNull(chapter1.getId()); //null before save
		chapterRepository.save(chapter1);
		assertNotNull(chapter1.getId()); //not null after save

		//fetch from DB
		Chapter fetchedChapter = chapterRepository.findOne(chapter1.getId());

		//should not be null
		assertNotNull(fetchedChapter);

		//should equal
		assertEquals(chapter1.getId(), fetchedChapter.getId());
		assertEquals(chapter1.getShortText(), fetchedChapter.getShortText());
	}

	@Test
	public void testSaveArticle(){
		Chapter chapter1 = new Chapter();
		chapter1.setShortText("Description 1 ");
		chapterRepository.save(chapter1);

		Article article1 = new Article();
		article1.setParentObject(chapter1);
		article1.setFullText("Full text");
		article1.setShortText("Short text");

		assertNull(article1.getId()); //null before save
		articleRepository.save(article1);
		assertNotNull(article1.getId()); //not null after save

		//fetch from DB
		Article fetchedArticle = articleRepository.findOne(article1.getId());

		//should not be null
		assertNotNull(fetchedArticle);

		//should equal
		assertEquals(article1.getId(), fetchedArticle.getId());
		assertEquals(article1.getShortText(), fetchedArticle.getShortText());
		assertEquals(article1.getFullText(), fetchedArticle.getFullText());
	}

}
