package springboot.services;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.domain.Article;
import springboot.domain.Chapter;
import springboot.repositories.ArticleRepository;

import java.util.List;

@Service
@Transactional
public class ArticleService implements IArticleService {
    @Autowired
    private SessionFactory mySessionFactory;

    @Transactional
    public void indexArticles() throws Exception {
        try {
            Session session = mySessionFactory.getCurrentSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);
            fullTextSession.createIndexer().startAndWait();
        } catch (Exception e) {
            throw e;
        }
    }

    private ArticleRepository articleRepository;
    @Autowired
    private IChapterService IChapterService;

    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }
    @Autowired
    public void setChapterService(IChapterService IChapterService) {
        this.IChapterService = IChapterService;
    }

    @Override
    public Article getArticleById(Integer id) {
        return articleRepository.findOne(id);
    }

    @Override
    public Article saveArticle(Article article) {
        Chapter parentChapter = IChapterService.getChapterById(article.getParentObject().getId());
        article.setParentObject(parentChapter);
        return articleRepository.save(article);
    }

    @Override
    public void deleteArticle(Integer id) {
        articleRepository.delete(id);
    }

    @Override public Article addArticle(Integer parentChapterId) {
        Chapter parentChapter = IChapterService.getChapterById(parentChapterId);
        Article article = new Article();
        article.setParentObject(parentChapter);

        saveArticle(article);
        return article;
    }

    @Override public List<Article> searchForArticles(String searchText) throws Exception
    {
        try
        {
            Session session = mySessionFactory.getCurrentSession();

            FullTextSession fullTextSession = Search.getFullTextSession(session);

            QueryBuilder qb = fullTextSession.getSearchFactory()
                .buildQueryBuilder().forEntity(Article.class).get();
            org.apache.lucene.search.Query query = qb
                .keyword().onFields("fullText")
                .matching(searchText)
                .createQuery();

            org.hibernate.Query hibQuery =
                fullTextSession.createFullTextQuery(query, Article.class);

            List<Article> results = hibQuery.list();
            return results;
        }
        catch(Exception e)
        {
            throw e;
        }
    }
}
