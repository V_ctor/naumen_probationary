package springboot.services;

import org.h2.jdbc.JdbcSQLException;
import org.springframework.dao.DataIntegrityViolationException;
import springboot.domain.Chapter;

public interface IChapterService {
    Iterable<Chapter> listAllChapter();

    Chapter getChapterById(Integer id);

    Chapter saveChapter(Chapter chapter);

    void deleteChapter(Integer id) throws DataIntegrityViolationException;

    Iterable<Chapter> listAllFirstLevelChapter();

    Chapter addChapter(Integer parentChapterId);
}
