package springboot.services;

import org.apache.log4j.Logger;
import org.h2.jdbc.JdbcSQLException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.domain.Chapter;
import springboot.repositories.ChapterRepository;

import java.util.Iterator;
import java.util.List;

@Service
@Transactional
public class ChapterService implements IChapterService {
    private ChapterRepository chapterRepository;

    protected static Logger logger = Logger.getLogger("ChapterServiceImpl");

    @Autowired
    public void setChapterRepository(ChapterRepository chapterRepository) {
        this.chapterRepository = chapterRepository;
    }

    @Override
    public Iterable<Chapter> listAllChapter() {
        return chapterRepository.findAll();
    }

    @Override
    public Chapter getChapterById(Integer id) {
        return chapterRepository.findOne(id);
    }

    @Override
    public Chapter saveChapter(Chapter chapter) {
        assert chapter.getParentChapter()!=null;
        Chapter parentChapter = (Chapter) getChapterById(chapter.getParentChapter().getId());    //this is not the best solution
        if (chapter.getId() != null) {      //null means new chapter
            chapter.setParentChapter(parentChapter);
        }
        return chapterRepository.save(chapter);
    }

    @Override
    public void deleteChapter(Integer id) throws DataIntegrityViolationException {
        chapterRepository.delete(id);
    }

    @Override
    public Iterable<Chapter> listAllFirstLevelChapter() {
        return chapterRepository.getFirstLevelChapters();
    }

    @Override public Chapter addChapter(Integer parentChapterId) {
        Chapter parentChapter = getChapterById(parentChapterId);
        Chapter chapter = new Chapter();
        chapter.setParentChapter(parentChapter);

        saveChapter(chapter);
        return chapter;
    }

}
