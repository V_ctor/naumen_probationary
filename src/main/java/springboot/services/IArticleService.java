package springboot.services;

import springboot.domain.Article;

import java.util.List;

public interface IArticleService {

    Article getArticleById(Integer id);

    Article saveArticle(Article article);

    void deleteArticle(Integer id);

    Article addArticle(Integer parentChapterId);

    void indexArticles() throws Exception;

    List<Article> searchForArticles(String searchText) throws Exception;
}
