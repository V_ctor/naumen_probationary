package springboot;

import org.h2.jdbc.JdbcSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import springboot.domain.Chapter;
import springboot.services.IChapterService;

@Controller
public class ChapterController {

    private IChapterService IChapterService;
    private Converter converter = new Converter() {
        @Override public Chapter convert(Object source) {
            assert source instanceof Integer;
            Integer id = (Integer)source;
            return IChapterService.getChapterById(id);        }
    };

    @Autowired
    public void setChapterService(IChapterService IChapterService) {
        this.IChapterService = IChapterService;
    }

    @RequestMapping(value = "/chapters", method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("chapters", IChapterService.listAllFirstLevelChapter());
        return "chapters::ChaptersFragment";
    }

    @RequestMapping(value = "/subChapters/{id}", method = RequestMethod.GET)
    public String showSubChapters(@PathVariable Integer id, Model model){
        Chapter chapter = IChapterService.getChapterById(id);
        model.addAttribute("chapters", chapter);
        model.addAttribute("isRecursiveEnabled", false);
        return "chapters::ChaptersFragment";
    }

    @RequestMapping("chapter/{id}")
    public String showChapter(@PathVariable Integer id, Model model){
        Chapter chapter = IChapterService.getChapterById(id);
        model.addAttribute("chapter", chapter);

        model.addAttribute("chapterPath", getChapterPath(chapter));
        return "chaptershow::chapterView";
    }

    private String getChapterPath(Chapter chapter) {
        StringBuilder sb= new StringBuilder(chapter.getShortText());
        if (chapter.getParentObject() == null) {
            sb.append("");
        }else {
            for (Chapter c = chapter;
                 c.getParentChapter().getParentChapter() != null;
                 c = c.getParentChapter()) {    //double .getParentChapter() because of virtual root parent
                sb.insert(0, "->");
                sb.insert(0, c.getParentChapter().getShortText());    //insert to 0 could be inefficient in deep levels
            }
        }
        return sb.toString();
    }

    @RequestMapping("chapter/edit/{id}")
    public String edit(@PathVariable Integer id,  Model model){
        Chapter chapter = IChapterService.getChapterById(id);
        model.addAttribute("chapter", chapter);
        model.addAttribute("chapterPath", getChapterPath(chapter));
        return "chapter_edit::chapterEdit";
    }

    @RequestMapping("chapter/new")
    public String newChapter(Model model){
        model.addAttribute("chapter", new Chapter());
        return "chapter_edit";
    }

    @RequestMapping("chapter/addChapter/{id}")
    public String add(@PathVariable Integer id,  Model model){
        Chapter chapter = IChapterService.addChapter(id);
        model.addAttribute("chapter", chapter);
        model.addAttribute("chapterPath", getChapterPath(chapter));
        return "chapter_edit::chapterEdit";
    }


    @RequestMapping(value = "chapter/save", method = RequestMethod.POST)
    public String saveChapter(Chapter chapter){
        assert chapter.getParentChapter()!=null;
        IChapterService.saveChapter(chapter);
        return "redirect:/chapter/" + chapter.getId();
    }

    @RequestMapping("chapter/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable Integer id){
        try {
            IChapterService.deleteChapter(id);
        }catch (DataIntegrityViolationException e){
            return "Error occured during deleting. Probably you have child chapters or/and articles? Try to delete them and try again.";
        }
        return "Chapter deleted!";
    }
}
