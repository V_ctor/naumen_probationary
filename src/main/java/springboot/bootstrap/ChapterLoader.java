package springboot.bootstrap;

import springboot.domain.Chapter;
import springboot.repositories.ChapterRepository;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
//import org.springframework.context.event.EventListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class ChapterLoader implements ApplicationListener<ContextRefreshedEvent> {

    private ChapterRepository chapterRepository;

    private Logger log = Logger.getLogger(ChapterLoader.class);

    @Autowired
    public void setChapterRepository(ChapterRepository chapterRepository) {
        this.chapterRepository = chapterRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

/*        Chapter chapter1 = new Chapter();
        chapter1.setShortText("Description 1 ");
        chapterRepository.save(chapter1);

        Chapter chapter1_1 = new Chapter();
        chapter1_1.setShortText("Description 1_1");
        chapter1_1.setParentChapter(chapter1);
        chapterRepository.save(chapter1_1);

        Chapter chapter1_1_1 = new Chapter();
        chapter1_1_1.setShortText("Description 1_1_1");
        chapter1_1_1.setParentChapter(chapter1_1);
        chapterRepository.save(chapter1_1_1);
        chapter1_1.addChildChapter(chapter1_1_1);*/
        /*
        chapterRepository.save(chapter1_1);
        chapter1.addChildChapter(chapter1_1);
        chapterRepository.save(chapter1);*/
        
    }
}
