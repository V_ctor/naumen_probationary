package springboot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
public class IndexController {

	@RequestMapping("/")  //TODO see http://www.thymeleaf.org/doc/articles/layouts.html
    String index(Principal principal){
        return "index";
    }
}
