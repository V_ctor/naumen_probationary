package springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springboot.domain.Article;
import springboot.domain.ITreeObject;
import springboot.services.IArticleService;
import springboot.services.IChapterService;

import java.util.List;

@Controller
public class ArticleController {

    private IArticleService IArticleService;
    private IChapterService IChapterService;

    @Autowired
    public void setArticleService(IArticleService IArticleService, IChapterService IChapterService) {
        this.IArticleService = IArticleService;
        this.IChapterService = IChapterService;
        try {
            IArticleService.indexArticles();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequestMapping("article/{id}")
    public String showArticle(@PathVariable Integer id, Model model){
        Article article = IArticleService.getArticleById(id);
        model.addAttribute("article", article);

        model.addAttribute("articlePath", getArticlePath(article));
        return "article_show::articleView";
    }

    private String getArticlePath(Article article) {
        StringBuilder sb= new StringBuilder(article.getShortText());
        for(ITreeObject c=article/*.getParentObject()*/;
            c.getParentObject().getParentObject()!=null;
            c=c.getParentObject()){    //double .getParentArticle() because of virtual root parent
            sb.insert(0, "->");
            sb.insert(0, c.getParentObject().getShortText());    //insert to 0 could be inefficient in deep levels
        }
        return sb.toString();
    }

    @RequestMapping("article/edit/{id}")
    public String edit(@PathVariable Integer id,  Model model){
        Article article = IArticleService.getArticleById(id);
        model.addAttribute("article", article);
        model.addAttribute("articlePath", getArticlePath(article));
        return "article_edit::articleEdit";
    }

    @RequestMapping("article/addArticle/{id}")
    public String add(@PathVariable Integer id,  Model model){

        Article article = IArticleService.addArticle(id);
        model.addAttribute("article", article);
        model.addAttribute("articlePath", getArticlePath(article));
        return "article_edit::articleEdit";
    }
    
    @RequestMapping("article/new")
    public String newArticle(Model model){
        model.addAttribute("article", new Article());
        return "article_edit";
    }

    @RequestMapping(value = "article/save", method = RequestMethod.POST)
    public String saveArticle(Article article){

        assert article.getParentObject()!=null;
        IArticleService.saveArticle(article);
        return "redirect:/article/" + article.getId();
    }

    @RequestMapping("article/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable Integer id){
        IArticleService.deleteArticle(id);
        return "Article deleted";
    }

    @RequestMapping("search")
    public String search(){
        return "search";
    }

    @RequestMapping(value = "search_text", method = RequestMethod.GET)
    public String search_text(@RequestParam("search_text") String searchText, Model model) {
        List<Article> articles = null;
        try {
            articles = IArticleService.searchForArticles(searchText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("articles", articles);
        return "articles::ArticlesFragment";
    }

}
