package springboot.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by victor on 04.10.15.
 */
@Entity
@Table(name = "CHAPTER")
public class Chapter implements ITreeObject{
    @Id
    @SequenceGenerator(name = "idGeneratorSeq", sequenceName = "idSequence", initialValue = 100)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "idGeneratorSeq")

    private Integer id;

    @Version
    private Integer version;

    @Column(name = "short_text", length = 128)
    private String shortText = "New Chapter";
    @OneToMany(mappedBy = "parentObject", /*orphanRemoval = true, */fetch = FetchType.LAZY)
    private List<Article> articleList;
    @OneToMany(mappedBy = "parentChapter", /*orphanRemoval = true, */fetch = FetchType.LAZY)
    private List<Chapter> childChapterList;

    @ManyToOne
    @JoinColumn(name = "parent_chapter_id")
    private Chapter parentChapter;

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    @Override public ITreeObject getParentObject() {
        return parentChapter;
    }

    @Override public void setParentObject(ITreeObject parentObject) {
        this.parentChapter = (Chapter) parentObject;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Chapter getParentChapter() {
        return parentChapter;
    }

    public void setParentChapter(Chapter parentChapter) {
        this.parentChapter = parentChapter;
    }

    public List<Chapter> getChildChapterList() {
        return childChapterList;
    }

    public void setChildChapterList(List<Chapter> childChapterList) {
        this.childChapterList = childChapterList;
    }

    public List<Article> getArticleList() {
        return articleList;
    }

    public void setArticleList(List<Article> articleList) {
        this.articleList = articleList;
    }

    public void addChildChapter(Chapter chapter){
        if (childChapterList==null)
            childChapterList = new ArrayList<>();
        childChapterList.add(chapter);
    }
    public Chapter getChildChapter(int index){
        assert (index > 0);
        assert childChapterList != null;
        return childChapterList.get(index);
    }

    public void addChildArticle(Article article) {
        articleList.add(article);
    }
}
