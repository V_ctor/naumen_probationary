package springboot.domain;

import javax.persistence.*;

@MappedSuperclass
public interface ITreeObject {

    String getShortText();

    void setShortText(String shortText);

    ITreeObject getParentObject();

    void setParentObject(ITreeObject parentObject);

    Integer getId();
    void setId(Integer id);
}
