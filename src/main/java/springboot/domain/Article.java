package springboot.domain;

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Parameter;

import javax.persistence.*;

@Entity
@Indexed
@AnalyzerDef(name = "ArticleAnalyzer",
    tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
    filters = {
        @TokenFilterDef(factory = LowerCaseFilterFactory.class),
        @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
            @Parameter(name = "language", value = "English")
        })
    })
public class Article implements ITreeObject {
    @Id
    @SequenceGenerator(name = "idGeneratorSeq", sequenceName = "idSequence", initialValue = 100)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "idGeneratorSeq")
    private Integer id;

    @Version
    private Integer version;

    @Column(name = "short_text")
    private String shortText="New article";
    @Column(name = "FULL_TEXT", length = 4096 )
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    @Analyzer(definition = "ArticleAnalyzer")
    private String fullText;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_OBJECT_ID")
    private Chapter parentObject;

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    @Override public Chapter getParentObject() {
        return parentObject;
    }

    @Override public void setParentObject(ITreeObject parentObject) {
        assert parentObject instanceof Chapter;
        this.parentObject = (Chapter) parentObject;
    }

}
