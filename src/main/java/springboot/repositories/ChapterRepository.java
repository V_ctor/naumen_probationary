package springboot.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import springboot.domain.Chapter;

import java.util.List;

public interface ChapterRepository extends CrudRepository<Chapter, Integer>{

    @Query("select c FROM  Chapter c where c.parentChapter is NULL")
    List<Chapter> getFirstLevelChapters();
}
