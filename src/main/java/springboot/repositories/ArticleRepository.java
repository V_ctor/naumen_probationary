package springboot.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import springboot.domain.Article;

import java.util.List;

public interface ArticleRepository extends CrudRepository<Article, Integer>{
}
