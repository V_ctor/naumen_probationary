INSERT INTO PUBLIC.CHAPTER(ID, SHORT_TEXT, VERSION, PARENT_CHAPTER_ID) VALUES
(1, 'Root', 0, NULL),
(2, 'Information technology 1_1', 0, 1),
(3, 'Description 1_2', 0, 2),
(4, 'Description 1_3', 0, 1),
(5, 'Data processing 1_1_1', 0, 2),
(6, 'Description 1_1_2', 0, 2),
(7, 'Description 1_1_1_1', 0, 5),
(8, 'Description 1_2_1', 0, 3);


INSERT INTO PUBLIC.ARTICLE(ID,  SHORT_TEXT, VERSION, PARENT_OBJECT_ID, FULL_TEXT  ) VALUES
(1,	'History of computer technology 1_1', 1, 2, 'Devices have been used to aid computation for thousands of years, probably initially in the form of a tally stick.[7] The Antikythera mechanism, dating from about the beginning of the first century BC, is generally considered to be the earliest known mechanical analog computer, and the earliest known geared mechanism.[8] Comparable geared devices did not emerge in Europe until the 16th century,[9] and it was not until 1645 that the first mechanical calculator capable of performing the four basic arithmetical operations was developed'),
(2, 'Data storage',	1, 5, 'Early electronic computers such as Colossus made use of punched tape, a long strip of paper on which data was represented by a series of holes, a technology now obsolete.[14] Electronic data storage, which is used in modern computers, dates from World War II, when a form of delay line memory was developed to remove the clutter from radar signals, the first practical application of which was the mercury delay line.[15] The first random-access digital storage device was the Williams tube, based on a standard cathode ray tube,[16] but the information stored in it and delay line memory was volatile in that it had to be continuously refreshed, and thus was lost once power was removed. The earliest form of non-volatile computer storage was the magnetic drum, invented in 1932[17] and used in the Ferranti Mark 1, the worlds first commercially available general-purpose electronic computer'),
(3,	'Data retrieval', 1, 5, 'The relational database model introduced a programming-language independent Structured Query Language (SQL), based on relational algebra. The terms "data" and "information" are not synonymous. Anything stored is data, but it only becomes information when it is organized and presented meaningfully.[29] Most of the worlds digital data is unstructured, and stored in a variety of different physical formats[30][b] even within a single organization. Data warehouses began to be developed in the 1980s to integrate these disparate stores. They typically contain data extracted from various sources, including external sources such as the Internet, organized in such a way as to facilitate decision support systems (DSS)');

